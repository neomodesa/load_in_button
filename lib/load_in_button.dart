import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:load_in_button/color_cupertino_activity_indicator.dart';

typedef LoadCallback = Future<bool> Function();

class LoadInButton extends StatefulWidget {

  final VoidCallback onPressed;
  final bool isLoading;
  final ValueChanged<bool> onHighlightChanged;
  final ButtonTextTheme textTheme;
  final Color textColor;
  final Color disabledTextColor;
  final Color color;
  final Color disabledColor;
  final Color focusColor;
  final Color hoverColor;
  final Color highlightColor;
  final Color splashColor;
  final Brightness colorBrightness;
  final double elevation;
  final double focusElevation;
  final double hoverElevation;
  final double highlightElevation;
  final double disabledElevation;
  final EdgeInsetsGeometry padding;
  final ShapeBorder shape;
  final Clip clipBehavior;
  final FocusNode focusNode;
  final MaterialTapTargetSize materialTapTargetSize;
  final Duration animationDuration;
  final Widget child;

  const LoadInButton({Key key, this.onPressed, this.isLoading, this.onHighlightChanged, this.textTheme, this.textColor, this.disabledTextColor, this.color,
    this.disabledColor, this.focusColor, this.hoverColor, this.highlightColor, this.splashColor, this.colorBrightness, this.elevation,
    this.focusElevation, this.hoverElevation, this.highlightElevation, this.disabledElevation, this.padding, this.shape, this.clipBehavior, this.focusNode, this.materialTapTargetSize, this.animationDuration, this.child}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LoadInButtonState();
}

class LoadInButtonState extends State<LoadInButton> {

  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: this.widget.isLoading ?? _isLoading ? () {} : this.widget.onPressed,
      onHighlightChanged: this.widget.onHighlightChanged,
      textTheme: this.widget.textTheme,
      textColor: this.widget.textColor,
      disabledTextColor: this.widget.disabledTextColor,
      color: this.widget.color,
      disabledColor: this.widget.disabledColor,
      focusColor: this.widget.focusColor,
      hoverColor: this.widget.hoverColor,
      highlightColor: this.widget.highlightColor,
      splashColor: this.widget.splashColor,
      colorBrightness: this.widget.colorBrightness,
      elevation: this.widget.elevation,
      focusElevation: this.widget.focusElevation,
      hoverElevation: this.widget.hoverElevation,
      highlightElevation: this.widget.highlightElevation,
      disabledElevation: this.widget.disabledElevation,
      padding: this.widget.padding,
      shape: this.widget.shape,
      clipBehavior: this.widget.clipBehavior ?? Clip.none,
      focusNode: this.widget.focusNode,
      materialTapTargetSize: this.widget.materialTapTargetSize,
      animationDuration: this.widget.animationDuration,
      child: (this.widget.isLoading ?? _isLoading) ? Center(
        child: SizedBox(
          width: 15, height: 15,
          child: _buildPlatformLoading(),
//          child: PlatformProgressIndicator(
//            materialValueColor: this.widget.textColor ?? Theme.of(context).textTheme.button.color,
//            materialStrokeWidth: 1.8,
//            cupertinoColor: this.widget.textColor ?? Theme.of(context).textTheme.button.color,
//          ),
        ),
      ) : this.widget.child,
    );
  }

  Widget _buildPlatformLoading() {
    if (Platform.isIOS) {
      return ColorCupertinoActivityIndicator(
        radius: 10.0,
        color: this.widget.textColor ?? Theme.of(context).textTheme.button.color,
      );
    } else {
      return CircularProgressIndicator(
        strokeWidth: 1.8,
        valueColor: AlwaysStoppedAnimation<Color>(this.widget.textColor ?? Theme.of(context).textTheme.button.color),
      );
    }
  }

  void start() {
    setState(() => _isLoading = true);
  }

  void finish() {
    setState(() => _isLoading = false);
  }

}