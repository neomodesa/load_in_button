## [0.0.2] - Fixed a bug where it was possible to tap while loading.
## [0.0.1] - First release of the load_in_button.
