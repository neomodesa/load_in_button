# load\_in\_button

A button with a loading icon inside.

## Getting Started

Just create the button on your code as the same way you would with a FlatButton.

```dart
LoadInButton(
  key: _buttonKey,
  onPressed: _buttonPressed,
  child: Text('CONFIRMAR PEDIDO'),
  color: Colors.red,
  textColor: Colors.white
)
```

### Control the loading

##### bool variable

`LoadInButton` contains a `isLoading` property which controls if the button is loading or not. Just set it's value on the widget build.

```dart
LoadInButton(
  key: _buttonKey,
  isLoading: _isButtonLoading,
  onPressed: _buttonPressed,
  child: Text('CONFIRMAR PEDIDO'),
  color: Colors.red,
  textColor: Colors.white
)
```

##### Acessing the state

It's also possible to use the `GlobalKey` to retrieve the start and use the helper methods

```dart
//Start the loading
_buttonKey.currentState?.start();

//Do something

//Finish the loading
_buttonKey.currentState?.finish();
```
